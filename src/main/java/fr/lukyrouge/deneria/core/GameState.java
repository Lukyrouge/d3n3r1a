package fr.lukyrouge.deneria.core;

public enum GameState {
    PRE_GAME, MATCH, POST_MATCH
}
