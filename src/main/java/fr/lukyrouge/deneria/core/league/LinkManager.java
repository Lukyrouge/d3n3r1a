package fr.lukyrouge.deneria.core.league;

import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.HashMap;

public class LinkManager extends ListenerAdapter {

    private static HashMap<User, LinkInstance> linkInstances = new HashMap<>();

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if (event.getMessage().getContentRaw().equalsIgnoreCase("ok")) {
            linkInstances.get(event.getAuthor()).setOksent(true);
        } else {
            if (linkInstances.getOrDefault(event.getAuthor(), null) != null) {
                linkInstances.get(event.getAuthor()).setSummonerName(event.getMessage().getContentRaw());
            }
        }
    }

    public void createLinkInstance(User u, String server) {
        linkInstances.put(u, new LinkInstance(u, System.currentTimeMillis(), server));
        Thread t = new Thread(linkInstances.get(u));
        t.start();
    }
}
