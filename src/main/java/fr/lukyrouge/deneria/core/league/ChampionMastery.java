package fr.lukyrouge.deneria.core.league;

public class ChampionMastery {

    private LeagueChampion champion;
    private int level, points;
    private int gameCount;

    public ChampionMastery(LeagueChampion champion, int level, int points, int gameCount) {
        this.champion = champion;
        this.level = level;
        this.points = points;
        this.gameCount = gameCount;
    }

    public int getGameCount() {
        return gameCount;
    }

    public void setGameCount(int gameCount) {
        this.gameCount = gameCount;
    }

    public LeagueChampion getChampion() {
        return champion;
    }

    public int getLevel() {
        return level;
    }

    public int getPoints() {
        return points;
    }
}
