package fr.lukyrouge.deneria.core.league;

public class LeagueMatch {

    private String lane, role;
    private long gameId, timestamp;
    private int champion, season, queue;

    public LeagueMatch(String lane, String role, long gmaeId, long timestamp, int champion, int season, int queue) {
        this.lane = lane;
        this.role = role;
        this.gameId = gmaeId;
        this.timestamp = timestamp;
        this.champion = champion;
        this.season = season;
        this.queue = queue;
    }

    public String getLane() {
        return lane;
    }

    public String getRole() {
        return role;
    }

    public long getGameId() {
        return gameId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getChampion() {
        return champion;
    }

    public int getSeason() {
        return season;
    }

    public int getQueue() {
        return queue;
    }
}
