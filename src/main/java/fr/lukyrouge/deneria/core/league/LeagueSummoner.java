package fr.lukyrouge.deneria.core.league;

import java.util.List;

public class LeagueSummoner {

    private int iconId;
    private String name;
    private String puuid;
    private long level;
    private String encryptedId;
    private String accountId;

    private List<LeagueEntry> leagueEntries;
    private List<ChampionMastery> championMasteries;
//    private List<LeagueMatch> matches;

    public LeagueSummoner(int iconId, String name, String puuid, long level, String encryptedId, String accountId, List<LeagueEntry> leagueEntries, List<ChampionMastery> championMasteries) {
        this.iconId = iconId;
        this.name = name;
        this.puuid = puuid;
        this.level = level;
        this.encryptedId = encryptedId;
        this.accountId = accountId;
        this.leagueEntries = leagueEntries;
        this.championMasteries = championMasteries;
    }


//    public List<LeagueMatch> getMatches() {
//        return matches;
//    }

    public int getIconId() {
        return iconId;
    }

    public String getName() {
        return name;
    }

    public String getPuuid() {
        return puuid;
    }

    public long getLevel() {
        return level;
    }

    public String getEncryptedId() {
        return encryptedId;
    }

    public String getAccountId() {
        return accountId;
    }

    public List<LeagueEntry> getLeagueEntries() {
        return leagueEntries;
    }

    public List<ChampionMastery> getChampionMasteries() {
        return championMasteries;
    }
}
