package fr.lukyrouge.deneria.core.league;

import fr.lukyrouge.deneria.api.LeagueOfLegends;

public class LeagueChampion {

    private String name;
    private int id;
    private String sprite;

    public LeagueChampion(String name, int id, String sprite) {
        this.name = name;
        this.id = id;
        this.sprite = sprite;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getSprite() {
        return "http://ddragon.leagueoflegends.com/cdn/" + LeagueOfLegends.LeagueVersion + "/img/champion/" + name + ".png";
    }
}
