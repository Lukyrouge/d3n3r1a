package fr.lukyrouge.deneria.core.league;

public class LeagueEntry {

    private String queueType, rank, leagueId, tier;
    private boolean hotStreak, veteran, inactive, freshBlood;
    private int wins, losses, leaguePoints;

    public LeagueEntry(String queueType, String rank, String leagueId, String tier, boolean hotStreak, boolean veteran, boolean inactive, boolean freshBlood, int wins, int losses, int leaguePoints) {
        this.queueType = queueType;
        this.rank = rank;
        this.leagueId = leagueId;
        this.tier = tier;
        this.hotStreak = hotStreak;
        this.veteran = veteran;
        this.inactive = inactive;
        this.freshBlood = freshBlood;
        this.wins = wins;
        this.losses = losses;
        this.leaguePoints = leaguePoints;
    }

    public String getQueueType() {
        return queueType;
    }

    public String getRank() {
        return rank;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public String getTier() {
        return tier;
    }

    public boolean isHotStreak() {
        return hotStreak;
    }

    public boolean isVeteran() {
        return veteran;
    }

    public boolean isInactive() {
        return inactive;
    }

    public boolean isFreshBlood() {
        return freshBlood;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getLeaguePoints() {
        return leaguePoints;
    }
}
