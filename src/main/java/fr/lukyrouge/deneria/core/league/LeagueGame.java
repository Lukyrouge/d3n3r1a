package fr.lukyrouge.deneria.core.league;

import java.util.List;

public class LeagueGame {

    private List<String> team;
    private int kills, deaths, assists, cs, duration, vision, champLevel;
    private long summoner1, summoner2;
    private LeagueChampion champion;
    private boolean penta, win, firstBlood;
    private List<Integer> items;
    private int queue;

    public LeagueGame(List<String> team, int kills, int deaths, int assists, int cs, int duration, int vision, int champLevel, long summoner1, long summoner2, LeagueChampion champion, boolean penta, boolean win, boolean firstBlood, List<Integer> items, int queue) {
        this.team = team;
        this.kills = kills;
        this.deaths = deaths;
        this.assists = assists;
        this.cs = cs;
        this.duration = duration;
        this.vision = vision;
        this.champLevel = champLevel;
        this.summoner1 = summoner1;
        this.summoner2 = summoner2;
        this.champion = champion;
        this.penta = penta;
        this.win = win;
        this.firstBlood = firstBlood;
        this.items = items;
        this.queue = queue;
    }

    public int getQueue() {
        return queue;
    }

    public List<Integer> getItems() {
        return items;
    }

    public long getSummoner2() {
        return summoner2;
    }

    public long getSummoner1() {
        return summoner1;
    }

    public int getChampLevel() {
        return champLevel;
    }

    public List<String> getTeam() {
        return team;
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getAssists() {
        return assists;
    }

    public int getCs() {
        return cs;
    }

    public int getDuration() {
        return duration;
    }

    public int getVision() {
        return vision;
    }

    public LeagueChampion getChampion() {
        return champion;
    }

    public boolean isPenta() {
        return penta;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isFirstBlood() {
        return firstBlood;
    }
}

