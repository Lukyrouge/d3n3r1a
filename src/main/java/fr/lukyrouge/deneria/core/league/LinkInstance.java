package fr.lukyrouge.deneria.core.league;

import fr.lukyrouge.deneria.api.LeagueOfLegends;
import fr.lukyrouge.deneria.database.DatabaseManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;

import java.util.Random;

public class LinkInstance implements Runnable {

    private static final long MAX_TIME = 120 * 1000;
    private static int ICON = new Random().nextInt(5);

    private User user;
    private long time;
    private String server;
    private String summonerName = "";
    private boolean firstResponse = false, oksent = false;
    private DatabaseManager manager;

    private String encryptedId = "";

    public LinkInstance(User user, long time, String server) {
        this.user = user;
        this.time = time;
        this.server = server;
        manager = new DatabaseManager();
    }

    @Override
    public void run() {
//        user.openPrivateChannel().queue(channel -> channel.sendMessage("Waiting...").queue());
        while (!Thread.currentThread().isInterrupted()) {
            if (!summonerName.equals("") && !firstResponse) {
                try {
                    encryptedId = LeagueOfLegends.getSummonerEncryptedId(summonerName, server);
                } catch (Exception e) {
                    user.openPrivateChannel().queue(c -> c.sendMessage("Summoner not found ! Aborting...").queue());
                    Thread.currentThread().interrupt();
                    break;
                }
                user.openPrivateChannel().queue((privateChannel -> {
                    privateChannel.sendMessage("Ok. Now change your summoner icon to this one : ").queue();
                    privateChannel.sendMessage(new EmbedBuilder()
                            .setImage("https://ddragon.leagueoflegends.com/cdn/9.16.1/img/profileicon/" + ICON + ".png").build()).queue();
                    privateChannel.sendMessage("And when it's done send me \"**OK**\"").queue();
                }));
                firstResponse = true;
                time = System.currentTimeMillis();
            }
            if (oksent && !encryptedId.equals("")) {
                try {
                    int iconId = LeagueOfLegends.getSummonerIcon(encryptedId, server);
                    if (iconId == ICON) {
                        manager.newLoLProfile(user.getIdLong(), encryptedId, server);
                        user.openPrivateChannel().queue(c -> c.sendMessage("Link done, you can set your icon back :)").queue());
                    } else user.openPrivateChannel().queue(c -> c.sendMessage("Wrong icon set. Aborting...").queue());
                    Thread.currentThread().interrupt();
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (System.currentTimeMillis() - time > MAX_TIME) {
                user.openPrivateChannel().queue((privateChannel -> {
                    privateChannel.sendMessage("Command timed out !").queue();
                }));
                Thread.currentThread().interrupt();
                break;
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }

    public void setOksent(boolean oksent) {
        if (firstResponse) this.oksent = oksent;
    }
}
