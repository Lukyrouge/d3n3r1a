package fr.lukyrouge.deneria.core;

import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Match implements Runnable {

    private static final String PLAYER1 = ":red_circle:";
    private static final String PLAYER2 = "\uD83D\uDD35";
    private static final String EMPTY = ":white_large_square:";
    private String[] words = {"zero", "one", "two", "three", "four", "five", "seven"};
    private String[] emojis = {"\u0031\u20E3", "\u0032\u20E3", "\u0033\u20E3", "\u0034\u20E3", "\u0035\u20E3", "\u0036\u20E3", "\u0037\u20E3"};

    private List<User> users;
    private GameState gameState;
    private int currentUser;
    private TextChannel channel;
    private Message reactMessage;
    private Message gameMessage;
    private String gameMessageId, reactMessageId;

    public TextChannel getChannel() {
        return channel;
    }

    public List<User> getUsers() {
        return users;
    }

    private int[] table = new int[49];

    public Match(List<User> users, TextChannel channel) {
        this.users = users;
        this.channel = channel;
        for (int i = 0; i < 49; i++) {
            table[i] = 0;
        }
    }

    @Override
    public void run() {
        currentUser = new Random().nextInt(100) > 50 ? 1 : 2;
//        currentUser = 2;
        StringBuilder msg = drawTable();
        MessageBuilder gmb = new MessageBuilder();
        gmb.setContent(msg.toString());
        gameMessage = gmb.build();
        gameMessageId = channel.sendMessage(gameMessage).complete().getId();
        MessageBuilder rmb = new MessageBuilder();
        rmb.setContent("It's " + users.get(currentUser - 1).getAsMention() + "'s turn");
        reactMessage = rmb.build();
        reactMessageId = channel.sendMessage(reactMessage).complete().getId();
        for (String w : emojis) {
            channel.addReactionById(reactMessageId, w).queue();
        }
        gameState = GameState.MATCH;
    }

    private StringBuilder drawTable() {
        StringBuilder msg = new StringBuilder();
        msg.append("Starting a new connect four game with ").append(users.get(0).getAsMention()).append(" and ").append(users.get(1).getAsMention());
        msg.append("\n :one::two::three::four::five::six::seven:");
        for (int i = 0; i < 49; i++) {
            if (i % 7 == 0) msg.append("\n ");
            if (table[i] == 0) msg.append(EMPTY);
            else msg.append(table[i] == 1 ? PLAYER1 : PLAYER2);
        }
//        msg.append("\nIt's ").append(users.get(currentUser-1).getAsMention()).append(" ").append(currentUser == 1 ? PLAYER1 : PLAYER2).append(" turn");
        return msg;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void reactionEvent(GuildMessageReactionAddEvent event) {
        if (gameState != GameState.MATCH) event.getChannel().sendMessage("There was an error owo").queue();
        else {
            if (event.getUser().equals(users.get(currentUser - 1))) {
                String name = event.getReactionEmote().getName();
                if (wordToNumber(name) != 0) {
                    int play = wordToNumber(name);
                    int start = play(play);
                    channel.editMessageById(gameMessageId, drawTable().toString()).queue();
                    if (play != -1) {
                        if (!isEnded(start)) {
                            currentUser = changeCurrentUser();
                            channel.editMessageById(reactMessageId, "It's " + users.get(currentUser - 1).getAsMention() + "'s turn").queue();
                        } else {
                            channel.sendMessage(users.get(currentUser - 1).getAsMention() + " has win !").queue();
                            gameState = GameState.POST_MATCH;
                            Thread.currentThread().interrupt();
                        }
                    }
//                    else {//TODO game ended}
                }
            }
        }
    }

    private int changeCurrentUser() {
        return currentUser == 1 ? 2 : 1;
    }

    private boolean isEnded(int start) {
        int starty = start / 7;
        int startx = start % 7;
        int hor1 = 0, hor2 = 0, ver1 = 0, ver2 = 0, diag1 = 0, diag2 = 0;

        int loc = start;

        //horizontal a droite
        while (table[loc] == currentUser) {
            hor1++;
            if (loc < 48) {
                loc++;
            } else break;
        }
        //horizontaL a gauche
        loc = start;
        while (table[loc] == currentUser) {
            hor2++;
            if (loc > 0) {
                loc--;
            } else break;
        }
        //vertical haut
        loc = start;
        while (table[loc] == currentUser) {
            ver1++;
            if (loc > 7) loc -= 7;
            else break;
        }
        //vertical bas
        loc = start;
        while (table[loc] == currentUser) {
            ver2++;
            if (loc < 42) loc += 7;
            else break;
        }
        //diag right

        loc = start;
        while (table[loc] == currentUser) {
            loc -= 6;

            if (loc < 6) break;
            else {
                diag1++;
            }
        }
        loc = start;
        while (table[loc] == currentUser) {
            loc += 6;

            if (loc > 42) break;
            else {
                diag1++;
            }
        }
        //diag left
        loc = start;
        while (table[loc] == currentUser) {
            loc -= 8;
            if (loc < 8) break;
            diag2++;
        }
        loc = start;
        while (table[loc] == currentUser) {
            loc += 8;

            if (loc > 41) break;
            diag2++;
        }
        int hor = hor1 + hor2 -1;
        int ver = ver1 + ver2 - 1;
        int diag = Math.max(diag1, diag2);
        return Math.max(Math.max(hor, ver), diag) >= 4;
    }

    private int play(int x1) {
        int x = x1 - 1;
        int y = 0;
        for (int y1 = 7; y1 > 1; y1--) {
            if (x + y1 * 7 < 49) if (table[x + y1 * 7] == 0) {
                y = y1;
                break;
            }
        }
        if (y != 0) table[x + y * 7] = currentUser;
        else return -1;
        return x + y * 7;
        //TODO : can not place
    }

    private int wordToNumber(String word) {
        for (int i = 0; i < emojis.length; i++) if (word.equals(emojis[i])) return i + 1;
        return 0;
    }
}