package fr.lukyrouge.deneria.core;

import fr.lukyrouge.deneria.logging.CustomLogger;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.VoiceChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static fr.lukyrouge.deneria.Constants.TIME_OUT;

public class VoiceChannelManager implements Runnable {

    Logger logger = LoggerFactory.getLogger(VoiceChannelManager.class);

    public HashMap<VoiceChannel, Member> memberChannels;
    public HashMap<VoiceChannel, Long> memberChannelsToDelete;
    public HashMap<VoiceChannel, Long> toRemove;

    public VoiceChannelManager() {
        memberChannels = new HashMap<>();
        memberChannelsToDelete = new HashMap<>();
        toRemove = new HashMap<>();
        logger.info("Initialized !");
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            for (Map.Entry<VoiceChannel, Member> mc : memberChannels.entrySet()) {
                if (mc.getKey().getMembers().size() == 0 && memberChannelsToDelete.get(mc.getKey()) == null) {
                    memberChannelsToDelete.put(mc.getKey(), System.currentTimeMillis());
                }
            }

            for (Map.Entry<VoiceChannel, Long> vl : memberChannelsToDelete.entrySet()) {
                long t = System.currentTimeMillis() - vl.getValue();
//                logger.info(String.valueOf(t));

                if (vl.getKey().getMembers().size() == 0 && t >= TIME_OUT) {
                    vl.getKey().delete().reason("Channel empty since " + t / 1000 + " seconds").queue();
                    memberChannels.remove(vl.getKey());
                    toRemove.put(vl.getKey(), vl.getValue());
                } else if ((vl.getKey().getMembers().size() > 0)) toRemove.put(vl.getKey(), vl.getValue());
            }

            for (Map.Entry<VoiceChannel, Long> vl : toRemove.entrySet())
                memberChannelsToDelete.remove(vl.getKey());
            toRemove.clear();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
