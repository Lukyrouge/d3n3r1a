package fr.lukyrouge.deneria.core;

import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public class MatchManager extends ListenerAdapter {

    private List<Match> matches = new ArrayList<>();

    public MatchManager() {

    }

    public void addNewMatch(Match match) {
        matches.add(match);
        Thread t = new Thread(match);
        t.start();
    }

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        List<Match> toRemove = new ArrayList<>();
        for (Match match : matches) {
            if (match.getGameState() == GameState.MATCH) {
                for (User user : match.getUsers()) {
                    if (event.getUser().equals(user) && event.getChannel().equals(match.getChannel())) {
                        match.reactionEvent(event);
                        break;
                    }
                }
            } else toRemove.add(match);
        }
        matches.removeAll(toRemove);
    }
}
