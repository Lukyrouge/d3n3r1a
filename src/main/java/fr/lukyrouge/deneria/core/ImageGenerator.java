package fr.lukyrouge.deneria.core;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import fr.lukyrouge.deneria.api.LeagueOfLegends;
import fr.lukyrouge.deneria.core.league.LeagueGame;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class ImageGenerator {

    private static Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
            "cloud_name", "dfeesxhtf",
            "api_key", "116546354255979",
            "api_secret", "gF_Y2qfKqrnLn1kVtPFNS88SWxU"));

    public static String getGameImage(List<LeagueGame> games) throws Exception {
        int width = 480;
        BufferedImage image = new BufferedImage(width, 130 * games.size(), BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2 = image.createGraphics();
        g2.setColor(new Color(150,150,150));
        g2.fillRect(0,0 , width, 130*games.size()-4);
        for (int i = 0; i < games.size(); i++) {
            g2.setColor(new Color(30,30,30));
            g2.fillRect(0, 130*i, width, 126);
            LeagueGame game = games.get(i);
            g2.drawImage(makeRoundedCorner(ImageIO.read(new URL("http://ddragon.leagueoflegends.com/cdn/" + LeagueOfLegends.LeagueVersion + "/img/champion/" + game.getChampion().getName() + ".png ")), width),  14, 14+ 130 * i, 100, 100, null);
            g2.setColor(Color.ORANGE);
            g2.drawOval(14, 14+130 * i, 100, 100);
            g2.setColor(Color.black);
            g2.fillOval(80, 80 + 130 * i, 40, 40);
            g2.setColor(Color.ORANGE);
            g2.drawOval(80, 80 + 130 * i, 40, 40);
            g2.setColor(Color.white);
            drawCenteredString(g2, String.valueOf(game.getChampLevel()), new Rectangle(90, 90 + 130 * i, 20, 20), new Font("TimesRoman", Font.BOLD, 20));
            if (game.isWin()) g2.setColor(Color.CYAN);
            else g2.setColor(new Color(255,35,69));
            drawCenteredString(g2, game.isWin() ? "VICTORY" : "DEFEAT", new Rectangle(132, 21 + 130 * i, 80, 20), new Font("TimesRoman", Font.BOLD, 20));
            g2.setColor(Color.white);
            //TODO
            drawCenteredString(g2, LeagueOfLegends.LeagueQueue.get(game.getQueue()), new Rectangle(107, 53 + 130 * i, 150, 12), new Font("TimesRoman", Font.BOLD, 12));
            g2.drawImage(ImageIO.read(new URL("http://ddragon.leagueoflegends.com/cdn/" + LeagueOfLegends.LeagueVersion + "/img/spell/" + LeagueOfLegends.getSummonerSpell(game.getSummoner1()) + ".png")), 130, 81 + 130 * i, 32, 32, null);
            g2.drawImage(ImageIO.read(new URL("http://ddragon.leagueoflegends.com/cdn/" + LeagueOfLegends.LeagueVersion + "/img/spell/" + LeagueOfLegends.getSummonerSpell(game.getSummoner2()) + ".png")), 162, 81 + 130 * i, 32, 32, null);
            g2.setColor(Color.pink);
            drawCenteredString(g2, String.format("%.2f", (((float) game.getAssists() + (float) game.getKills()) / (float) game.getDeaths())) + " KDA", new Rectangle(231, 21 + 130 * i, 80, 15), new Font("TimesRoman", Font.BOLD, 15));
            g2.setColor(Color.white);
            drawCenteredString(g2, game.getKills() + " / " + game.getDeaths() + " / " + game.getAssists(), new Rectangle(231, 45 + 130 * i, 80, 10), new Font("TimesRoman", Font.BOLD, 14));
            drawCenteredString(g2, String.valueOf(game.getCs() / (game.getDuration() / 60)) + " CS/min.", new Rectangle(317, 21 + 130 * i, 80, 10), new Font("TimesRoman", Font.BOLD, 14));
            drawCenteredString(g2, (int) Math.floor((float) game.getDuration() / 60) + ":" + game.getDuration() % 60, new Rectangle(400, 21 + 130 * i, 80, 10), new Font("TimesRoman", Font.BOLD, 14));
            drawCenteredString(g2, game.getCs() + " CS", new Rectangle(317, 45 + 130 * i, 80, 10), new Font("TimesRoman", Font.BOLD, 14));
            for (int j = 0; j < game.getItems().size(); j++) {
                if (game.getItems().get(j) != 0)
                    g2.drawImage(ImageIO.read(new URL("http://ddragon.leagueoflegends.com/cdn/" + LeagueOfLegends.LeagueVersion + "/img/item/" + game.getItems().get(j) + ".png")), 240 + 32 * j, 81 + 130 * i, 32, 32, null);
            }
        }
        g2.dispose();
        File output = new File("img.png");
        ImageIO.write(image, "png", output);
        Map uploadResult = cloudinary.uploader().upload("img.png", ObjectUtils.emptyMap());
        return (String) uploadResult.get("url");
    }

    public static BufferedImage makeRoundedCorner(BufferedImage image, int cornerRadius) {
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = output.createGraphics();

        // This is what we want, but it only does hard-clipping, i.e. aliasing
        // g2.setClip(new RoundRectangle2D ...)

        // so instead fake soft-clipping by first drawing the desired clip shape
        // in fully opaque white with antialiasing enabled...
        g2.setComposite(AlphaComposite.Src);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.WHITE);
        g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));

        // ... then compositing the image on top,
        // using the white shape from above as alpha source
        g2.setComposite(AlphaComposite.SrcAtop);
        g2.drawImage(image, 0, 0, null);

        g2.dispose();

        return output;
    }

    /**
     * Draw a String centered in the middle of a Rectangle.
     *
     * @param g    The Graphics instance.
     * @param text The String to draw.
     * @param rect The Rectangle to center the text in.
     */
    public static void drawCenteredString(Graphics g, String text, Rectangle rect, Font font) {
        // Get the FontMetrics
        FontMetrics metrics = g.getFontMetrics(font);
        // Determine the X coordinate for the text
        int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        // Determine the Y coordinate for the text (note we add the ascent, as in java 2d 0 is top of the screen)
        int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        // Set the font
        g.setFont(font);
        // Draw the String
        g.drawString(text, x, y);
    }
}
