package fr.lukyrouge.deneria;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.Command.Category;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.Nekos;
import fr.lukyrouge.deneria.commands.InfoCommand;
import fr.lukyrouge.deneria.commands.admin.ClearCommand;
import fr.lukyrouge.deneria.commands.admin.VoiceChannelCommand;
import fr.lukyrouge.deneria.commands.fun.InterractCommand;
import fr.lukyrouge.deneria.commands.fun.LMGTFYCommand;
import fr.lukyrouge.deneria.commands.game.ConnectFourCommand;
import fr.lukyrouge.deneria.commands.lol.*;
import fr.lukyrouge.deneria.commands.nsfw.NSFWCommand;
import fr.lukyrouge.deneria.commands.pictures.NekoCommand;
import fr.lukyrouge.deneria.commands.pictures.NekoGCommand;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.Consumer;

public class CommandRegister {

    private CommandClientBuilder client;
    private ArrayList<Command> commands = new ArrayList<>();
    private Consumer<CommandEvent> helpConsumer;

    public static final Category ADMIN = new Category("Admin :bust_in_silhouette: ");
//    public static final Category MUSIC = new Category("Music \uD83C\uDFB5 "); ABORTED
    public static final Category LOL = new Category("League of Legends");
    public static final Category PICTURES = new Category("Pictures :heart_eyes_cat:");
    public static final Category NSFW = new Category("NSFW :smirk:");
    public static final Category FUN = new Category("Fun :joy:");
    public static final Category GAME = new Category("Game :video_game:");


    public CommandRegister(CommandClientBuilder client) {
        this.client = client;
    }

    public void registerCommands() {
        if (commands.size() == 0) {
            commands.add(new NekoCommand());
            commands.add(new ClearCommand());
            commands.add(new NekoGCommand());
            commands.add(new InterractCommand("tickle", "Tickle someone", "tickling"));
            commands.add(new InterractCommand("poke", "Poke someone", "poking"));
            commands.add(new InterractCommand("hug", "Hug someone", "hugging"));
            commands.add(new InterractCommand("slap", "Slap someone", "slapping"));
            commands.add(new InterractCommand("baka", "Baka someone", "baka"));
            commands.add(new InterractCommand("kiss", "Kiss someone", "kissing"));
            commands.add(new InterractCommand("cuddle", "Cuddle someone", "cuddling"));
            commands.add(new InterractCommand("pat", "Pat someone", "patting"));
            commands.add(new InterractCommand("feed", "Feed someone", "feeding"));
            commands.add(new LMGTFYCommand());
            commands.add(new ConnectFourCommand());


            commands.add(new InfoCommand());
            commands.add(new NSFWCommand("lewd", "Random lewd owo"));
            commands.add(new NSFWCommand("femdom", "Random femdom owo"));
            commands.add(new NSFWCommand("feet", "Random ero feet owo", new String[]{"erofeet"}));
            commands.add(new NSFWCommand("erok", "Random ero kitsune owo", new String[]{"erokitsune"}));
            commands.add(new NSFWCommand("hlewd", "Random holo lewd owo", new String[]{"hololewd"}));
            commands.add(new NSFWCommand("les", "Random les owo", new String[]{"onlyg"}));
            commands.add(new NSFWCommand("klewd", "Random lewd kitsune owo", new String[]{"lewdk", "lewdkitsune"}));
            commands.add(new NSFWCommand("classic", "Random classic owo", new String[]{"fuck"}));
            commands.add(new NSFWCommand("eron", "Random ero neko owo"));

            commands.add(new ProfileCommand());
            commands.add(new MasteriesCommand());
            commands.add(new ProgressCommand());
            commands.add(new LinkCommand());
            commands.add(new UnlinkCommand());
            commands.add(new VoiceChannelCommand());

            commands.sort(Comparator.comparing(o -> o.getCategory().getName()));

            helpConsumer = event -> {
                EmbedBuilder embed = new EmbedBuilder();
                event.reactSuccess();
//                embed.setTitle("** DenriaBot Help ** :book:", null);
                embed.setAuthor("DeneriaBot Help \uD83D\uDCD6", null, "https://cdn.nekos.life/avatar/avatar_08.png");
                embed.setColor(Color.white);
                Category category = null;
                StringBuilder str = new StringBuilder();
                for (Command cmd : commands) {
                    if (cmd.getCategory() != NSFW || event.getTextChannel().isNSFW()) {
                        if (!cmd.isHidden() && (!cmd.isOwnerCommand() || event.isOwner())) {
                            if (!Objects.equals(category, cmd.getCategory())) {
                                if (str.toString() != "" && category != null)
                                    embed.addField(category.getName(), str.toString(), false);
                                category = cmd.getCategory();
                                str = new StringBuilder();
                            }
                            str.append("\n **-").append(cmd.getName()).append("**: ").append(cmd.getHelp());
                            if (cmd.getAliases() != null && cmd.getAliases().length > 0) {
                                str.append("\n**Aliases**: `");
                                boolean first = true;
                                for (String s : cmd.getAliases()) {
                                    if (!first) str.append(", ");
                                    str.append(s);
                                    first = false;
                                }
                                str.append("`");
                            }
                            if (cmd.getArguments() != "" && cmd.getArguments() != null) {
                                str.append("\n**Arguments**: `").append(cmd.getArguments()).append("`");
                            }
                        }
                    }
                }
                if (str.toString() != "" && category != null) embed.addField(category.getName(), str.toString(), false);
                try {
                    embed.setFooter("Help command was asked by " + event.getMember().getEffectiveName() + " " + Nekos.getCat(), null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                event.reply(embed.build());
            };
            client.setHelpConsumer(helpConsumer);

            for (Command cmd : commands) {
                client.addCommand(cmd);
            }
        }
    }

    public static void lewdError(String name, CommandEvent event) {
        EmbedBuilder embed = new EmbedBuilder();
        try {
            embed.setDescription(Nekos.getCat() + name + " are only for +18 people ! Please use this command only in nsfw channel " + Nekos.getCat());
            embed.setColor(Color.pink);
            event.reply(embed.build());
            event.reactWarning();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
