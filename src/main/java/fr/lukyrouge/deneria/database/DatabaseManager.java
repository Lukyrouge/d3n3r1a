package fr.lukyrouge.deneria.database;

import fr.lukyrouge.deneria.database.models.LoLProfile;
import fr.lukyrouge.deneria.logging.CustomLogger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class DatabaseManager {

    private DeneriaDatabaseModel model = null;
    private CustomLogger log = new CustomLogger(LoggerFactory.getLogger(DatabaseManager.class));

    public DatabaseManager() {
        try {
            model = new DeneriaDatabaseModel();
        } catch (SQLException | ClassNotFoundException | NoSuchFieldException e) {
            log.error(e.getMessage());
        }
    }

    public LoLProfile getLoLProfile(long discordId, String server) {
        List<LoLProfile> profiles = null;
        try {
            profiles = model.getObjectModel(LoLProfile.class).getAll("discord_id = ? AND server = ?", discordId, server);
        } catch (SQLException ignored) {
        }
        return profiles.size() >= 1 ? profiles.get(0) : null;
    }

    public LoLProfile getLoLProfile(long discordId) {
        List<LoLProfile> profiles = null;
        try {
            profiles = model.getObjectModel(LoLProfile.class).getAll("discord_id = ?", discordId);
        } catch (SQLException ignored) {
        }
        return profiles.size() >= 1 ? profiles.get(0) : null;
    }

    public void newLoLProfile(long discordId, String summonerId, String server) throws SQLException {
        LoLProfile profile = new LoLProfile(discordId, summonerId, server);
        List<LoLProfile> profiles = model.getObjectModel(LoLProfile.class).getAll();
        profile.setId(profiles.size() > 0 ? profiles.get(profiles.size()-1).getId() + 1 : 0);
        System.out.println(model.getObjectModel(LoLProfile.class).insertAndReturnUpdated(profile));
    }

    public void deleteLoLProfile(LoLProfile profile) throws SQLException {
        model.getObjectModel(LoLProfile.class).delete(profile);
    }

    public void dispose() {
        if (model != null) {
            model.disconnect();
            model = null;
        }
    }

}
