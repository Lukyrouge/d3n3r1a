package fr.lukyrouge.deneria.database.models;

import fr.lukyrouge.deneria.database.DBModel;
import za.co.neilson.sqlite.orm.annotations.PrimaryKey;
import za.co.neilson.sqlite.orm.annotations.Unique;

public class LoLProfile extends DBModel {

    @PrimaryKey
    private int id;
    private long discord_id;
    private String summoner_id;
    private String server;

    public LoLProfile(long discord_id, String summoner_id, String server) {
        this.discord_id = discord_id;
        this.summoner_id = summoner_id;
        this.server = server;
    }

    public LoLProfile() {
    }

    public int getId() {
        return id;
    }

    public String getServer() {
        return server;
    }

    public long getDiscord_id() {
        return discord_id;
    }

    public String getSummoner_id() {
        return summoner_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDiscord_id(long discord_id) {
        this.discord_id = discord_id;
    }

    public void setSummoner_id(String summoner_id) {
        this.summoner_id = summoner_id;
    }
}
