package fr.lukyrouge.deneria.database;

import fr.lukyrouge.deneria.database.models.LoLProfile;
import za.co.neilson.sqlite.orm.DatabaseDriverInterface;
import za.co.neilson.sqlite.orm.DatabaseInfo;
import za.co.neilson.sqlite.orm.DatabaseModel;
import za.co.neilson.sqlite.orm.ObjectModel;
import za.co.neilson.sqlite.orm.jdbc.JdbcObjectModel;
import za.co.neilson.sqlite.orm.jdbc.JdbcSqliteDatabaseDriverInterface;

import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import static fr.lukyrouge.deneria.Constants.*;
import static fr.lukyrouge.deneria.database.DatabaseEnum.LolProfile;

public class DeneriaDatabaseModel extends DatabaseModel<ResultSet, HashMap<String, Object>> {

    public DeneriaDatabaseModel(Object... args) throws SQLException, ClassNotFoundException, NoSuchFieldException {
        super(null);
    }

    @Override
    public ObjectModel<DatabaseInfo, ResultSet, HashMap<String, Object>> onCreateDatabaseInfoModel() throws ClassNotFoundException, NoSuchFieldException {
        return new JdbcObjectModel<DatabaseInfo>(this) {
        };
    }

    @Override
    protected DatabaseDriverInterface<ResultSet, HashMap<String, Object>> onInitializeDatabaseDriverInterface(Object... objects) {
        return new JdbcSqliteDatabaseDriverInterface(this);
    }

    @Override
    protected void onRegisterObjectModels(HashMap<Type, ObjectModel<?, ResultSet, HashMap<String, Object>>> hashMap) throws ClassNotFoundException, NoSuchFieldException {
        objectModels.put(LoLProfile.class, new JdbcObjectModel<LoLProfile>(this) {
        });
    }

    @Override
    public String getDatabaseName() {
        return "database.db";
    }

    @Override
    public int getDatabaseVersion() {
        return 1;
    }

    @Override
    protected void onInsertDefaultValues() {
    }

}
