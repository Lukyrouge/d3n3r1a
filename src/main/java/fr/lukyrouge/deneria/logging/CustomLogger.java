package fr.lukyrouge.deneria.logging;

import fr.lukyrouge.deneria.D3n3r1a;
import net.dv8tion.jda.core.entities.TextChannel;
import org.slf4j.Logger;

import static fr.lukyrouge.deneria.Constants.LOG_CHANNEL;

public class CustomLogger {

    private Logger log;

    public CustomLogger(Logger log) {
        this.log = log;
    }

    public void info(String msg) {
        log.info(msg);
        LOG_CHANNEL.sendMessage("[INFO] " + getName() + " - " + msg).complete();
    }

    public void error(String msg) {
        log.error(msg);
        LOG_CHANNEL.sendMessage("[ERROR] " + getName() + " - " + msg).complete();
    }

    private String getName() {
        String[] name = log.getName().split("\\.");
        return name[name.length - 1];
    }
}
