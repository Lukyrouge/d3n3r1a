package fr.lukyrouge.deneria.commands.pictures;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.Nekos;
import net.dv8tion.jda.core.EmbedBuilder;

import static fr.lukyrouge.deneria.CommandRegister.PICTURES;

public class NekoGCommand extends Command {

    public NekoGCommand() {
        this.name = "nekog";
        this.help = "Random neko gif owo";
        this.arguments = "<count>";
        this.aliases = new String[]{"nekogif"};
        this.category = PICTURES;
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        int count = 1;
        if (!commandEvent.getArgs().isEmpty()) {
            count = Integer.parseInt(commandEvent.getArgs().split("\\s+")[0]);
        }
        if (count > 10) count = 10;
        for (int i = 0; i < count; i++) {
            EmbedBuilder embed = new EmbedBuilder();
            try {
                embed.setDescription(Nekos.getNekoG());
                embed.setImage(Nekos.getNeko());
                commandEvent.reply(embed.build());
                commandEvent.reactSuccess();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
