package fr.lukyrouge.deneria.commands.pictures;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.Nekos;
import net.dv8tion.jda.core.EmbedBuilder;

import static fr.lukyrouge.deneria.CommandRegister.PICTURES;

public class NekoCommand extends Command {

    public NekoCommand() {
        this.name = "neko";
        this.help = "Random neko owo";
        this.arguments = "<count>";
        this.category = PICTURES;
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        int count = 1;
        if (!commandEvent.getArgs().isEmpty()) {
            count = Integer.parseInt(commandEvent.getArgs().split("\\s+")[0]);
        }
        if (count > 10) count = 10;
        for (int i = 0; i < count; i++) {
            EmbedBuilder embed = new EmbedBuilder();
            try {
                embed.setDescription(Nekos.getCat());
                embed.setImage(Nekos.getNeko());
                commandEvent.reply(embed.build());
                commandEvent.reactSuccess();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
