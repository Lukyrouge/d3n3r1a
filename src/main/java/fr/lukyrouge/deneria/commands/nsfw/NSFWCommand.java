package fr.lukyrouge.deneria.commands.nsfw;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.Nekos;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;

import static fr.lukyrouge.deneria.CommandRegister.NSFW;
import static fr.lukyrouge.deneria.CommandRegister.lewdError;

public class NSFWCommand extends Command {

    public NSFWCommand(String name, String help, String[] aliases, String arguments, Category category) {
        this.name = name;
        this.help = help;
        this.arguments = arguments;
        this.aliases = aliases;
        this.category = category;
    }

    public NSFWCommand(String name, String help, String[] aliases, String arguments) {
        this(name, help, aliases,arguments, NSFW);
    }

    public NSFWCommand(String name, String help, String[] aliases) {
        this(name, help, aliases, "<count>");
    }

    public NSFWCommand(String name, String help) {
        this(name, help, new String[]{});
    }

    @Override
    protected void execute(CommandEvent event) {
        if (event.getTextChannel().isNSFW()) {
            int count = 1;
            if (!event.getArgs().isEmpty()) {
                count = Integer.parseInt(event.getArgs().split("\\s+")[0]);
            }
            if (count > 10) count = 10;
            for (int i = 0; i < count; i++) {
                EmbedBuilder embed = new EmbedBuilder();
                try {
                    embed.setDescription(Nekos.getCat());
                    embed.setImage(Nekos.get(name));
                    embed.setColor(Color.white);
                    event.reply(embed.build());
                    event.reactSuccess();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            lewdError("Nsfw things", event);
        }
    }
}
