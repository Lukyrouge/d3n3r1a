package fr.lukyrouge.deneria.commands.lol;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.D3n3r1a;
import fr.lukyrouge.deneria.api.LeagueOfLegends;
import fr.lukyrouge.deneria.database.DatabaseManager;
import fr.lukyrouge.deneria.database.models.LoLProfile;
import fr.lukyrouge.deneria.logging.CustomLogger;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Random;

import static fr.lukyrouge.deneria.CommandRegister.LOL;

public class LinkCommand extends Command {

    private DatabaseManager manager;
    private CustomLogger log = new CustomLogger(LoggerFactory.getLogger(LinkCommand.class));


    public LinkCommand() {
        this.name = "link";
        this.help = "Link you discord profile with your LoL summoner";
        this.arguments = "<server>";
        this.category = LOL;
        this.cooldown = 120;
        this.cooldownScope = CooldownScope.USER;

    }

    @Override
    protected void execute(CommandEvent event) {
        String server = "euw";
        if (event.getArgs().length() > 0)  server = event.getArgs();
        User user = event.getAuthor();
        manager = new DatabaseManager();

        LoLProfile profile = manager.getLoLProfile(user.getIdLong(), server);
        if (profile != null) {
            try {
                event.replyError("Already linked with " + LeagueOfLegends.getSummonerName(profile.getSummoner_id(), "euw") + ". To unlink please use `unlink` command.");
            } catch (Exception ignored) {
            }
        } else {
            user.openPrivateChannel().queue((channel) -> channel.sendMessage("Ok I see you want to link you discord profile with your League of Legends summoner.\n" +
                    "To start the procedure send me the summoner name you want to link. \n" +
                    "(You have 2 minutes until this time out)").queue());
            D3n3r1a.linkManager.createLinkInstance(user, server);
            event.reply("I sent you the informations in private message !");
            event.reactSuccess();
        }

    }
}
