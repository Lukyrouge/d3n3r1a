package fr.lukyrouge.deneria.commands.lol;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.LeagueOfLegends;
import fr.lukyrouge.deneria.core.*;
import fr.lukyrouge.deneria.core.league.*;
import fr.lukyrouge.deneria.database.DatabaseManager;
import fr.lukyrouge.deneria.database.models.LoLProfile;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import javax.xml.crypto.Data;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.lukyrouge.deneria.CommandRegister.LOL;

public class ProfileCommand extends Command {

    private DatabaseManager manager;

    public ProfileCommand() {
        this.name = "profile";
        this.help = "Check someone's lol profile";
        this.arguments = "<name> (server)";
        this.aliases = new String[]{"profiletr", "profileeuw", "profileus"};
        this.category = LOL;
        this.cooldown = 60;
        this.cooldownScope = CooldownScope.USER;

        manager = new DatabaseManager();
    }

    @Override
    protected void execute(CommandEvent event) {
        Thread t = new Thread(() -> {
            String name = "";
            String server = "euw";
            server = event.getMessage().getContentRaw().contains("profiletr") ? "tr" : server;
            server = event.getMessage().getContentRaw().contains("profileus") ? "us" : server;
            long start = System.currentTimeMillis();
            LoLProfile profile = event.getMessage().getMentionedMembers().size() > 0 ? manager.getLoLProfile(event.getMessage().getMentionedMembers().get(0).getUser().getIdLong(), server) : manager.getLoLProfile(event.getAuthor().getIdLong(), server);
//            System.out.println(profile != null ? profile.getSummoner_id() + " " + profile.getServer() + " " + profile.getDiscord_id() : "null");
            if (profile != null) {
                try {
                    name = LeagueOfLegends.getSummonerName(profile.getSummoner_id(), server);
                } catch (Exception ignored) {
                }
            } else if (!event.getArgs().

                    isEmpty() && event.getMessage().

                    getMentionedMembers().

                    size() == 0) {
                String[] arr = event.getArgs().split("\\s+");
                for (String s : arr) name += s;
            }  else {
                event.reply("Please provide a summoner name ! ");
                event.reactError();
                return;
            }
            try {
                Message msg = event.getTextChannel().sendMessage("Analysing...").complete();
                event.getTextChannel().sendTyping().complete();
                LeagueSummoner summoner = LeagueOfLegends.getSummoner(name, server);

                EmbedBuilder embed = new EmbedBuilder();
                embed.setAuthor(summoner.getName() + "'s profile", null, LeagueOfLegends.getIcon(summoner.getIconId()));

                StringBuilder rank = new StringBuilder();
                for (LeagueEntry entry : summoner.getLeagueEntries()) {
                    float total = (entry.getLosses() + entry.getWins());
                    String r = entry.getTier().toLowerCase();
                    rank.append("**").append(LeagueOfLegends.getLeagueQueueName(entry.getQueueType())).append("** : ").
                            append(r.substring(0, 1).toUpperCase()).append(r.substring(1)).append(" ").
                            append(entry.getRank()).append(" (").append(entry.getWins()).append("/").
                            append(entry.getLosses()).append(" - ").append(String.format("%.2f", (entry.getWins() / total) * 100)).
                            append("%").append(")").append("\n");
                }
                embed.addField("Ranks", rank.toString(), false);
                StringBuilder mastery = new StringBuilder();
                for (int i = 0; i < 3; i++) {
                    ChampionMastery champ = summoner.getChampionMasteries().get(i);
                    int matches = LeagueOfLegends.getMatchNumber(summoner.getAccountId(), server, champ.getChampion().getId());
                    mastery.append("**").append(champ.getChampion().getName()).append("** : ").
                            append("Mastery ").append(champ.getLevel()).append(" (").
                            append(LeagueOfLegends.formatPoints(champ.getPoints(), 0)).
                            append(" - ").append(matches).append(" matches").
                            append(")\n");
                }

                embed.addField("Best Masteries", mastery.toString(), false);
                List<LeagueMatch> gameMatches = LeagueOfLegends.getSummonerMatches(summoner.getAccountId(), server, 3);
                List<LeagueGame> games = new ArrayList<>();
                for (LeagueMatch match : gameMatches) {
                    LeagueGame game = LeagueOfLegends.getLeagueGame(match.getGameId(), summoner.getName(), server);
                    games.add(game);
                }
                embed.setImage(ImageGenerator.getGameImage(games));

                embed.addField("Last 3 matches", "", false);
                long end = System.currentTimeMillis();
                embed.setFooter("Time taken " + ((int) ((end - start) / 1000)) + "s", null);
                msg.editMessage("Analysed !").complete().editMessage(embed.build()).complete();
                Thread.currentThread().interrupt();
                //                event.reactSuccess();
            } catch (
                    Exception e) {
                //                e.printStackTrace();
                System.out.println(e.getMessage());
                event.replyError("Summoner not found !");
                Thread.currentThread().interrupt();
            }

        });
        t.start();
    }
}
