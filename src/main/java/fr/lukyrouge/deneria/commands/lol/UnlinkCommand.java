package fr.lukyrouge.deneria.commands.lol;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.D3n3r1a;
import fr.lukyrouge.deneria.database.DatabaseManager;
import fr.lukyrouge.deneria.database.models.LoLProfile;
import fr.lukyrouge.deneria.logging.CustomLogger;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

import static fr.lukyrouge.deneria.CommandRegister.LOL;

public class UnlinkCommand extends Command {

    private DatabaseManager manager;
    private CustomLogger log = new CustomLogger(LoggerFactory.getLogger(LinkCommand.class));


    public UnlinkCommand() {
        this.name = "unlink";
        this.help = "Unlink you discord profile with your LoL summoner";
        this.arguments = "";
        this.category = LOL;
    }
    @Override
    protected void execute(CommandEvent event) {
        User user  = event.getAuthor();
        if (event.getMember().isOwner()) {
            manager = new DatabaseManager();
            try {
                LoLProfile profile = manager.getLoLProfile(user.getIdLong());
                if (profile == null)
                    event.replyError("Not linked with a summoner. To link please use `link` command.");
                else {
                    manager.deleteLoLProfile(profile);
                    event.reply("Successfully unlinked !");
                    event.reactSuccess();
                }
            } catch (SQLException e) {
                log.error(e.getSQLState());
            }
        } else {
            event.reply(new EmbedBuilder().setDescription("Finished ~" + ProgressCommand.progress(55)).build());
        }
    }
}
