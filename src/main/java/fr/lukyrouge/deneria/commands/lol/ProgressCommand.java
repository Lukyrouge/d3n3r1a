package fr.lukyrouge.deneria.commands.lol;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;

import java.util.Random;

import static fr.lukyrouge.deneria.CommandRegister.LOL;

public class ProgressCommand extends Command {
    private static final char EMPTY = '░';
    private static final char FULL = '█';

    public ProgressCommand() {
        this.name = "progress";
        this.help = "a";
        this.arguments = "";
        this.category = LOL;
    }

    @Override
    protected void execute(CommandEvent event) {
        int perc = new Random().nextInt(100);
        EmbedBuilder embed = new EmbedBuilder();
        embed.addField("Perc", progress(perc), false);
        event.reply(embed.build());
    }

    public static String progress(int perc) {
        String progress = "";
        int full = perc / 10;
        int empty = 10 - full;
        for (int i = 0; i < full; i++) progress += FULL;
        for (int i = 0; i < empty; i++) progress += EMPTY;
        progress += " " + perc + "%";
        return progress;
    }
}
