package fr.lukyrouge.deneria.commands.lol;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.LeagueOfLegends;
import fr.lukyrouge.deneria.core.league.ChampionMastery;
import fr.lukyrouge.deneria.core.league.LeagueMatch;
import fr.lukyrouge.deneria.core.league.LeagueSummoner;
import fr.lukyrouge.deneria.database.DatabaseManager;
import fr.lukyrouge.deneria.database.models.LoLProfile;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.util.List;

import static fr.lukyrouge.deneria.CommandRegister.LOL;

public class MasteriesCommand extends Command {
    private DatabaseManager manager;

    public MasteriesCommand() {
        this.name = "masteries";
        this.help = "Check someone's lol masteries";
        this.arguments = "<name> (server)";
        this.aliases = new String[] {"master"};
        this.category = LOL;

        manager = new DatabaseManager();

    }

    @Override
    protected void execute(CommandEvent event) {
        String name = "";
        String server = "euw";
        server = event.getMessage().getContentRaw().contains("masteriestr") ? "tr" : server;
        server = event.getMessage().getContentRaw().contains("masteriesus") ? "us" : server;
        Message msg = event.getTextChannel().sendMessage("Analysing...").complete();

        LoLProfile profile = event.getMessage().getMentionedMembers().size() > 0 ? manager.getLoLProfile(event.getMessage().getMentionedMembers().get(0).getUser().getIdLong(), server) : manager.getLoLProfile(event.getAuthor().getIdLong(), server);


        long start = System.currentTimeMillis();
        if (profile != null) {
            try {
                name = LeagueOfLegends.getSummonerName(profile.getSummoner_id(), server);
            } catch (Exception ignored) {
            }
        } else if (!event.getArgs().isEmpty()) {
            String[] arr = event.getArgs().split("\\s+");
            for (String s : arr) name += s;
        } else {
            event.replyError("Please provide a username !");
        }
        try {
            LeagueSummoner summoner = LeagueOfLegends.getSummoner(name, server);
            List<LeagueMatch> matches = LeagueOfLegends.getSummonerMatches(summoner.getAccountId(), server, 10000);
            EmbedBuilder embed = new EmbedBuilder();
            embed.setAuthor(summoner.getName() + "'s masteries", null, LeagueOfLegends.getIcon(summoner.getIconId()));

            StringBuilder mastery = new StringBuilder();
            int max = 7;
            for (int i = 0; i < Math.min(summoner.getChampionMasteries().size(), max); i++) {
                ChampionMastery champ = summoner.getChampionMasteries().get(i);
                long count = matches.stream().filter(m -> m.getChampion() == champ.getChampion().getId()).count();
                mastery.append("**").append(champ.getChampion().getName()).append("** : ").
                        append("Mastery ").append(champ.getLevel()).append(" (").
                        append(LeagueOfLegends.formatPoints(champ.getPoints(), 0)).
                        append(" - ").append(count).append(" matches").
                        append(")\n");
            }
            embed.addField("Masteries", mastery.toString(), false);
            long end =  System.currentTimeMillis();
            embed.setFooter("Time taken " + (int) ((end - start)/1000) + "s", null);
            msg.editMessage("Analysed !").complete().editMessage(embed.build()).complete();
            msg.addReaction("✅").complete();
        } catch (Exception e) {
            e.printStackTrace();
            event.replyError("Summoner not found !");
        }
    }

}
