package fr.lukyrouge.deneria.commands.admin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.D3n3r1a;
import fr.lukyrouge.deneria.core.VoiceChannelManager;
import fr.lukyrouge.deneria.logging.CustomLogger;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.VoiceChannel;
import org.slf4j.LoggerFactory;

import static fr.lukyrouge.deneria.CommandRegister.ADMIN;

public class VoiceChannelCommand extends Command {

    CustomLogger logger = new CustomLogger(LoggerFactory.getLogger(VoiceChannelCommand.class));
    VoiceChannelManager vcm = D3n3r1a.voiceChannelManager;

    public VoiceChannelCommand() {
        this.name = "voicechannel";
        this.hidden = true;
        this.category = ADMIN;
        this.aliases = new String[]{"chat"};
    }

    @Override
    protected void execute(CommandEvent event) {
        Member m = event.getMember();
        if (vcm.memberChannels.getOrDefault(m, null) == null) {
            VoiceChannel vc = (VoiceChannel) m.getGuild().getCategoryById("490955681835384878").createVoiceChannel(m.getEffectiveName() + "'s channel").complete();
            vcm.memberChannels.put(vc, m);
        }
    }
}
