package fr.lukyrouge.deneria.commands.admin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;

import java.util.List;

import static fr.lukyrouge.deneria.CommandRegister.ADMIN;

public class ClearCommand extends Command {

    public ClearCommand() {
        this.name = "clear";
        this.arguments = "<count>";
        this.category = ADMIN;
        this.help = "Clears the number of messages provided";
    }

    @Override
    protected void execute(CommandEvent event) {
        if (event.getMember().hasPermission(Permission.MESSAGE_MANAGE)) {
            if (event.getArgs().isEmpty()) event.replyError("You didn't give me the number to clear uwu");
            else {
                Thread t = new Thread(() -> {
                    int count = Integer.parseInt(event.getArgs().split("\\s+")[0]);
                    if (count > 100) count = 100;
                    if (count < 0) count = 1;
                    event.replySuccess("Deleting " + count + " messages... owo");
                    List<Message> msgs = event.getChannel().getHistoryBefore(event.getMessage(), count).complete().getRetrievedHistory();
//                    for (Message msg : msgs) {
//                        msg.delete().queue();
//                    }
                    event.getTextChannel().deleteMessages(msgs).queue();
                    msgs = event.getChannel().getHistory().retrievePast(2).complete();
                    for (Message msg : msgs) {
                        msg.delete().queue();
                    }
                });
                t.start();
            }
        } else event.replyError("You can't do that uwu");
//        event.reactSuccess();
    }
}
