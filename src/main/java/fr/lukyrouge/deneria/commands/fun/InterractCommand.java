package fr.lukyrouge.deneria.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.Nekos;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;

import static fr.lukyrouge.deneria.CommandRegister.FUN;

public class InterractCommand extends Command {
    String action;

    public InterractCommand(String name, String help, String action, String[] aliases, String arguments, Category category) {
        this.name = name;
        this.help = help;
        this.aliases = aliases;
        this.arguments = arguments;
        this.category = category;
        this.action = action;
    }

    public InterractCommand(String name, String help, String action, String[] aliases, String arguments) {
        this(name, help, action, aliases, arguments, FUN);
    }

    public InterractCommand(String name, String help, String action, String[] aliases) {
        this(name, help, action, aliases, "<name>");
    }

    public InterractCommand(String name, String help, String action) {
        this(name, help, action, new String[]{});
    }

    @Override
    protected void execute(CommandEvent event) {
        String name = "";
        if (!event.getMessage().getMentionedMembers().isEmpty()) {
            name = event.getMessage().getMentionedMembers().get(0).getEffectiveName();
        } else if (!event.getArgs().isEmpty()) {
            name = event.getArgs().split("\\s+")[0];
        }
        EmbedBuilder embed = new EmbedBuilder();
        try {
            embed.setDescription(Nekos.getCat());
            embed.setImage(Nekos.get(this.name));
            embed.setFooter("I am " + this.action + " " + name + " for you owo", null);
            embed.setColor(Color.white);
            event.reply(embed.build());
            event.reactSuccess();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
