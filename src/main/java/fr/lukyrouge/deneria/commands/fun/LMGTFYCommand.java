package fr.lukyrouge.deneria.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.api.Nekos;

import static fr.lukyrouge.deneria.CommandRegister.FUN;

public class LMGTFYCommand extends Command {

    public LMGTFYCommand() {
        this.name = "lmgtfy";
        this.help = "Let me search for you";
        this.aliases = new String[] {"search", "google"};
        this.arguments = "<search>";
        this.category = FUN;
    }

    @Override
    protected void execute(CommandEvent event) {
        try {
            if (!event.getArgs().isEmpty()) {
                String search = "";
                int lie = 1, i = 0;
                String[] args = event.getArgs().split("\\s+");
                if (args[0].equalsIgnoreCase("false")) {
                    lie = 0;
                    i = 1;
                } else if (args[0].equalsIgnoreCase("true")) i = 1;
                for (int j = i; j<args.length; j++) {
                    search += args[j] + "+";
                }

                String url = "http://lmgtfy.com/?iie=" + lie + "&q=" + search;
                event.replySuccess("Here it is owo : " + url);
            } else event.replyError("You didn't tell me what to search ! " + Nekos.getCat());
        } catch (Exception ignored) {}
    }
}
