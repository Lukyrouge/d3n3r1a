//package fr.lukyrouge.deneria.commands.game;
//
//import com.jagrosh.jdautilities.command.CommandEvent;
//import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
//import net.dv8tion.jda.core.MessageBuilder;
//import net.dv8tion.jda.core.entities.Member;
//import net.dv8tion.jda.core.entities.Message;
//import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
//
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.atomic.AtomicBoolean;
//
//public class ConnectFour implements Runnable {
//
//    private static final String PLAYER1 = ":red_circle:";
//    private static final String PLAYER2 = ":blue_circle:";
//    private static final String EMPTY = ":white_large_square:";
//
//    private final CommandEvent event;
//    private final EventWaiter waiter;
//    private Member player1;
//    private Member player2;
//    private Member currentPlayer;
//
//    public ConnectFour(CommandEvent event, EventWaiter waiter) {
//        this.event = event;
//        this.waiter = waiter;
//    }
//
//    @Override
//    public void run() {
//        if (!event.getArgs().isEmpty()) { //Check if arguments are given
//            List<Member> members = event.getMessage().getMentionedMembers();
////            List<Member> members = new ArrayList<>(membersro);
//            if (members.size() == 1) { // check is there is 2 members mentionned
////                members.add(event.getMember());
////                int randomplayer = new Random().nextInt(2);
////                int randomplayer2 = randomplayer == 2 ? 0 : 1;
////                player1 = members.get(randomplayer-1);
//                player1 = event.getMember();
//                player2 = members.get(0);
//                if (player1 == player2) return;
//
//                // 0 = void, 1 = player 1, 2 = player 2
//                int[] table = new int[49];
//                //Fill the table with 0
//                for (int i = 0; i < 49; i++) {
//                    table[i] = 0;
//                }
//                currentPlayer = player1;
//
//                StringBuilder msg = drawTable(table);
//                AtomicBoolean gameRunning = new AtomicBoolean(true);
//                MessageBuilder mb = new MessageBuilder();
//                mb.setContent(msg.toString());
//                final Message gameMessage = mb.build();
//                String gameMessageId = event.getChannel().sendMessage(gameMessage).complete().getId();
////                event.reply(gameMessage);
//                final Member finalCurrentPlayer = currentPlayer;
//                while(gameRunning.get()) {
//                    waiter.waitForEvent(MessageReceivedEvent.class,
//                            e -> e.getMember().equals(finalCurrentPlayer) && e.getChannel().equals(event.getChannel()),
//                            e -> {
//                                if (e.getMessage().getContentRaw().matches("\\d+")) {
//                                    int place = Integer.parseInt(e.getMessage().getContentRaw());
//                                    if (place >= 1 && place <= 9) {
////                                        playerPlay(e, place, table, finalCurrentPlayer, finalCurrentPlayer == player1 ? 1 : 2, gameMessageId);
//                                    }
//                                }
//                            },
//                            1, TimeUnit.MINUTES, () -> {
//                                event.replyError("You take too long to play.");
//                            });
////                    if (gameRunning.get()) {
////                        currentPlayer = currentPlayer == player1 ? player2 : player1;
////                    }
//                }
//            } else {
//                event.replyError("You have to mention 1 user to play with you");
//                Thread.currentThread().interrupt();
//            }
//        } else {
//            event.replyError("You have to mention 1 user to play with you");
//            Thread.currentThread().interrupt();
//        }
//
//    }
//
//    private boolean waitForPlay(Member finalCurrentPlayer, AtomicBoolean gameRunning, int[] table, String gameMessageId) {
//
//        return gameRunning.get();
//    }
//
//    private StringBuilder drawTable(int[] table) {
//        StringBuilder msg = new StringBuilder();
//        msg.append("Starting a new connect four game with ").append(player1.getAsMention()).append(" and ").append(player2.getAsMention());
//        msg.append("\n :one::two::three::four::five::six::seven:");
//        for (int i = 0; i < 49; i++) {
//            if (i % 7 == 0) msg.append("\n ");
//            if (table[i] == 0) msg.append(EMPTY);
//            else msg.append(table[i] == 1 ? PLAYER1 : PLAYER2);
//        }
//        msg.append("\nIt's ").append(currentPlayer.getAsMention()).append(" ").append(currentPlayer == player1 ? PLAYER1 : PLAYER2).append(" turn");
//        return msg;
//    }
//
//    private boolean playerPlay(MessageReceivedEvent e, int x, int[] table, Member player, int playernumber, String gameMessageId) {
//        int y = -1;
//        int loc = -1;
//        for (int i = 6; i > 1; i--) {
//            loc = (x-1)+ i * 7;
//            if (table[loc] == 0) {
//                table[loc] = playernumber;
//                y = i;
//                break;
//            }
//        }
//        System.out.println("Playing loc: " + loc + " x: " + (x-1) + " y: " + y);
//        if (y == -1 || loc == -1) e.getMessage().addReaction(":x:").queue(); //TODO: wait an another call
//        else {
//            //Check if win
//            if (countTokenTo(table, playernumber, loc) >= 4) {
//                //it's win
//                return false;
//            } else {
//                e.getTextChannel().editMessageById(gameMessageId, drawTable(table)).queue();
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private int countTokenTo(int[] table, int playernumber, int start) {
//
//    }
//}
