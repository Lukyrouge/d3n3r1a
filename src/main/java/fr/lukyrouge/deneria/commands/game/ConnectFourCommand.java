package fr.lukyrouge.deneria.commands.game;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import fr.lukyrouge.deneria.core.Match;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.List;

import static fr.lukyrouge.deneria.CommandRegister.GAME;
import static fr.lukyrouge.deneria.D3n3r1a.matchManager;

public class ConnectFourCommand extends Command {

    public ConnectFourCommand() {
        this.name = "connectfour";
        this.help = "Start a connect four party";
        this.aliases = new String[]{"puissance4", "p4"};
        this.category = GAME;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!event.getMessage().getMentionedMembers().isEmpty()) {
            List<User> users = new ArrayList<>();
            users.add(event.getMessage().getMentionedUsers().get(0));
            users.add(event.getAuthor());
            matchManager.addNewMatch(new Match(users, event.getTextChannel()));
        } else event.replyError("You didn't tell me with who you want to play :/");
    }
}
