package fr.lukyrouge.deneria.listener;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import fr.lukyrouge.deneria.logging.CustomLogger;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class CustomCommandListener implements com.jagrosh.jdautilities.command.CommandListener {
    private CustomLogger log = new CustomLogger(LoggerFactory.getLogger("CommandListener"));

    private HashMap<User, Long> userCooldown = new HashMap<>();

    @Override
    public void onCompletedCommand(CommandEvent event, Command command) {
        if (command == null) {
            log.info("Command `help` by " + event.getAuthor().getName() + " in guild `" + event.getGuild().getName() + "`");
            return;
        }
        log.info("Command `" + getCommand(event) + "` by " + event.getAuthor().getName() + " in guild `" + event.getGuild().getName() + "`");
        if (!event.getMember().isOwner()) userCooldown.put(event.getAuthor(), System.currentTimeMillis());
    }

    private String getCommand(CommandEvent event) {
        return event.getMessage().getContentRaw().split("!")[0].equalsIgnoreCase("d") ? event.getMessage().getContentRaw().substring(2) : event.getMessage().getContentRaw().substring(7);
    }
}
