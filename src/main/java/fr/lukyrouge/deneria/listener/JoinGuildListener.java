package fr.lukyrouge.deneria.listener;

import fr.lukyrouge.deneria.logging.CustomLogger;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.slf4j.LoggerFactory;

public class JoinGuildListener extends ListenerAdapter {

    CustomLogger logger = new CustomLogger(LoggerFactory.getLogger(JoinGuildListener.class));

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        logger.info("Joined " + event.getGuild().getName() + " guild (" +event.getGuild().getMembers().size()+" members)");
    }
}
