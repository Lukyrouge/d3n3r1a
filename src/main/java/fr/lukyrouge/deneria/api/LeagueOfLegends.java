package fr.lukyrouge.deneria.api;

import fr.lukyrouge.deneria.core.league.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static fr.lukyrouge.deneria.Constants.LEAGUE_KEY;

public class LeagueOfLegends {
    private static final OkHttpClient client = new OkHttpClient();

    public static HashMap<Long, LeagueChampion> LeagueChampions;
    public static HashMap<Long, String> LeagueSummoners;
    public static HashMap<Integer, String> LeagueQueue = new HashMap<Integer, String>() {
        {
            put(0, "Custom game");put(72, "1v1 Snowdown Showdown");put(73, "2v2 Snowdown Showdown");put(75, "6v6 Hexakill");put(76, "Ultra Rapid Fire");put(78, "One For All: Mirror Mode");put(83, "Co-op vs AI Ultra Rapid Fire");put(98, "6v6 Hexakill");put(100, "ARAM");put(310, "Nemesis");put(313, "Black Market Brawlers");put(317, "Definitely Not Dominion");put(325, "All Random");put(400, "Normal Draft Pick");put(420, "Ranked Solo/Duo");put(430, "Blind Pick");put(440, "Ranked Flex");put(450, "ARAM");put(460, "3v3 Blind Pick");put(470, "3v3 Ranked Flex");put(600, "Blood Hunt Assassin");put(610, "Dark Star: Singularity");put(700, "Clash");put(800, "Co-op vs. AI Intermediate Bot");put(810, "Co-op vs. AI Intro Bot");put(820, "Co-op vs. AI Beginner Bot");put(830, "Co-op vs. AI Intro Bot");put(840, "Co-op vs. AI Beginner Bot");put(850, "Co-op vs. AI Intermediate Bot");put(900, "ARURF");put(910, "Ascension");//To be continued; https://developer.riotgames.com/game-constants.html
        }
    };
    public static String LeagueVersion;

    public static String getIcon(int id) throws Exception {
        return "http://ddragon.leagueoflegends.com/cdn/" + LeagueVersion + "/img/profileicon/" + id + ".png";
    }

    public static String getVersion() {
//        URL url = null;
//        try {
//            url = new URL("https://ddragon.leagueoflegends.com/api/versions.json");
//            JSONTokener tokener = new JSONTokener(url.openStream());
//            JSONArray root = new JSONArray(tokener);
//            return root.get(0).toString();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
        Request request = new Request.Builder().url("https://ddragon.leagueoflegends.com/api/versions.json").build();
        try {
            Response res = client.newCall(request).execute();
//            System.out.println(res.body().string());
            try (ResponseBody body = res.body()) {
                if(!res.isSuccessful()) throw new IOException("shit, req failed with "+res+" wsi down again?");
                JSONArray root = new JSONArray(Objects.requireNonNull(body).string());
                return root.get(0).toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "9.16.1";
    }

    public static LeagueSummoner getSummoner(String name) throws Exception {
        return LeagueOfLegends.getSummoner(name, "euw");
    }

    public static List<LeagueEntry> getSummonerEntries(String id, String server) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/league/v4/entries/by-summoner/" + id + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONArray arr = new JSONArray(tokener);
        List<LeagueEntry> entries = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject j = arr.getJSONObject(i);
//            System.out.println("LeagueId " + j.getString("leagueId"));
            entries.add(new LeagueEntry(j.getString("queueType"), j.getString("rank"), j.getString("leagueId"),
                    j.getString("tier"), j.getBoolean("hotStreak"), j.getBoolean("veteran"),
                    j.getBoolean("inactive"), j.getBoolean("freshBlood"), j.getInt("wins"),
                    j.getInt("losses"), j.getInt("leaguePoints")));
        }
        return entries;
    }

    public static List<ChampionMastery> getSummonerMasteries(String id, String server, String accountId) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/champion-mastery/v4/champion-masteries/by-summoner/" + id + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONArray arr = new JSONArray(tokener);
        List<ChampionMastery> entries = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject j = arr.getJSONObject(i);
            entries.add(new ChampionMastery(getChampion(j.getLong("championId")),
                    j.getInt("championLevel"), j.getInt("championPoints"), 0));
        }
        return entries;
    }


    public static int getMatchNumber(String id, String server, int champion) {
        try {
            URL url = new URL("https://" + server + "1.api.riotgames.com/lol/match/v4/matchlists/by-account/" + id + "?champion=" + champion + "&api_key=" + LEAGUE_KEY);
            JSONTokener tokener = new JSONTokener(url.openStream());
            JSONObject root = new JSONObject(tokener);

            return root.getInt("totalGames");
        } catch (Exception e) {
            return 0;
        }
    }

    public static List<LeagueMatch> getSummonerMatches(String id, String server, int champion, int count) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/match/v4/matchlists/by-account/" + id + "?champion=" + champion + "&api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject root = new JSONObject(tokener);
        JSONArray arr = root.getJSONArray("matches");
        List<LeagueMatch> entries = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject j = arr.getJSONObject(i);
            entries.add(new LeagueMatch(j.getString("lane"), j.getString("role"),
                    j.getInt("gameId"), j.getLong("timestamp"), j.getInt("champion"),
                    j.getInt("season"), j.getInt("queue")));
        }
        return entries;
    }

    public static List<LeagueMatch> getSummonerMatches(String id, String server, int count) throws Exception {

        List<LeagueMatch> entries = new ArrayList<>();
        for (int c = 0; c < Math.ceil((float) count / 100); c++) {
            URL url = new URL("https://" + server + "1.api.riotgames.com/lol/match/v4/matchlists/by-account/" + id + "?beginIndex=" + c * 100 + "&api_key=" + LEAGUE_KEY);
            JSONTokener tokener = new JSONTokener(url.openStream());
            JSONObject root = new JSONObject(tokener);
            JSONArray arr = root.getJSONArray("matches");
            if (root.getInt("endIndex") == root.getInt("startIndex")) break;
            for (int i = 0; i < arr.length(); i++) {
                JSONObject j = arr.getJSONObject(i);
                String lane = "";
                String role = "";
                if (j.has("lane")) {
                    lane = j.getString("lane");
                } else System.out.println(j);
                if (j.has("role")) role = j.getString("role");
                entries.add(new LeagueMatch(lane, role,
                        j.getLong("gameId"), j.getLong("timestamp"), j.getInt("champion"),
                        j.getInt("season"), j.getInt("queue")));
            }
        }
        return entries.subList(0, Math.min(count, entries.size()));
    }

    public static LeagueSummoner getSummoner(String name, String server) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/summoner/v4/summoners/by-name/" + name + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject json = new JSONObject(tokener);
        String encryptedId = json.getString("id");
        return new LeagueSummoner(json.getInt("profileIconId"), json.getString("name"),
                json.getString("puuid"), json.getLong("summonerLevel"),
                encryptedId, json.getString("accountId"),
                getSummonerEntries(encryptedId, server), getSummonerMasteries(encryptedId, server, json.getString("accountId")));
    }

    public static Integer getSummonerIcon(String encryptedId, String server) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/summoner/v4/summoners/" + encryptedId + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject json = new JSONObject(tokener);
        return json.getInt("profileIconId");
    }

    public static String getSummonerName(String encryptedId, String server) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/summoner/v4/summoners/" + encryptedId + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject json = new JSONObject(tokener);
        return json.getString("name");
    }

    public static String getSummonerEncryptedId(String name, String server) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/summoner/v4/summoners/by-name/" + name + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject json = new JSONObject(tokener);
        return json.getString("id");
    }

    public static LeagueGame getLeagueGame(long gameId, String name, String server) throws Exception {
        URL url = new URL("https://" + server + "1.api.riotgames.com/lol/match/v4/matches/" + gameId + "?api_key=" + LEAGUE_KEY);
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject root = new JSONObject(tokener);
        List<String> team = new ArrayList<>();
        int participantId = 0;
        for (int i = 0; i < root.getJSONArray("participantIdentities").length(); i++) {
            String n = root.getJSONArray("participantIdentities").getJSONObject(i).getJSONObject("player").getString("summonerName");
            team.add(n);
            if (n.equalsIgnoreCase(name))
                participantId = root.getJSONArray("participantIdentities").getJSONObject(i).getInt("participantId");
        }
        int duration = (int) root.getLong("gameDuration");
        JSONArray participants = root.getJSONArray("participants");
        int kills, deaths, assists, cs, vision, champLevel;
        long summoner1, summoner2;
        boolean win, penta, firstBlood;
        LeagueChampion champion;
        List<Integer> items = new ArrayList<>();
        for (int i = 0; i < participants.length(); i++) {
            JSONObject part = participants.getJSONObject(i);
            JSONObject stats = part.getJSONObject("stats");
            if (part.getInt("participantId") == participantId) {
                kills = stats.getInt("kills");
                deaths = stats.getInt("deaths");
                assists = stats.getInt("assists");
                win = stats.getBoolean("win");
                penta = stats.getInt("pentaKills") > 0;
                firstBlood = stats.getBoolean("firstBloodAssist") || stats.getBoolean("firstBloodKill");
                cs = stats.getInt("neutralMinionsKilled") + stats.getInt("totalMinionsKilled");
                vision = stats.getInt("visionScore");
                champion = getChampion(part.getLong("championId"));
                champLevel = stats.getInt("champLevel");
                summoner1 = part.getLong("spell1Id");
                summoner2 = part.getLong("spell2Id");
                for (int j = 0; j < 6; j++) {
                    items.add(stats.getInt("item" + j));
                }
                return new LeagueGame(team, kills, deaths, assists, cs, duration, vision, champLevel, summoner1, summoner2, champion, penta, win, firstBlood, items, root.getInt("queueId"));
            }
        }
        return null;
    }

    private static HashMap<String, String> QueueNameType = new HashMap<String, String>() {
        {
            put("RANKED_SOLO_5x5", "Solo/Duo");
            put("RANKED_TEAM_5x5", "Flex 5v5");
            put("RANKED_TEAM_3x3", "Flex 3v3");
            put("RANKED_FLEX_SR", "Flex");
            put("RANKED_TFT", "Team Fight Tactics");
        }
    };

    public static String getLeagueQueueName(String queueType) {
        return QueueNameType.get(queueType) != null ? QueueNameType.get(queueType) : queueType;
    }

    public static HashMap<Long, LeagueChampion> getChampions(String version) throws Exception {
        HashMap<Long, LeagueChampion> champions = new HashMap<>();
        URL url = new URL("http://ddragon.leagueoflegends.com/cdn/" + version + "/data/en_US/champion.json");
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject root = new JSONObject(tokener);
        JSONObject arr = root.getJSONObject("data");
        for (String key : arr.keySet()) {
            JSONObject o = arr.getJSONObject(key);
            LeagueChampion champ = new LeagueChampion(o.getString("id"), o.getInt("key"), o.getJSONObject("image").getString("sprite"));
            champions.put(o.getLong("key"), champ);
        }
        return champions;
    }

    public static HashMap<Long, String> getSummoners(String version) throws Exception {
        HashMap<Long, String> summoners = new HashMap<>();
        URL url = new URL("http://ddragon.leagueoflegends.com/cdn/" + version + "/data/en_US/summoner.json");
        JSONTokener tokener = new JSONTokener(url.openStream());
        JSONObject root = new JSONObject(tokener);
        JSONObject arr = root.getJSONObject("data");
        for (String key : arr.keySet()) {
            JSONObject o = arr.getJSONObject(key);
            summoners.put(o.getLong("key"), o.getString("id"));
        }
        return summoners;
    }

    public static LeagueChampion getChampion(long id) {
        return LeagueChampions.get(id);
    }

    public static String getSummonerSpell(long id) {
        return LeagueSummoners.get(id);
    }

    public static void initVersion() {
        LeagueVersion = getVersion();
    }

    public static void initChamp() throws Exception {
        LeagueChampions = getChampions(LeagueVersion);
    }

    public static void initSummoners() throws Exception {
        LeagueSummoners = getSummoners(LeagueVersion);
    }

    private static char[] c = new char[]{'k', 'm', 'b', 't'};

    public static String formatPoints(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000 ? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : formatPoints(d, iteration + 1));

    }

    public static int countMatches(List<LeagueMatch> matches, int champion) {
        return (int) matches.stream().filter(m -> m.getChampion() == champion).count();
    }
}
