package fr.lukyrouge.deneria;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import fr.lukyrouge.deneria.api.LeagueOfLegends;
import fr.lukyrouge.deneria.core.VoiceChannelManager;
import fr.lukyrouge.deneria.core.league.LinkManager;
import fr.lukyrouge.deneria.database.DeneriaDatabaseModel;
import fr.lukyrouge.deneria.database.models.LoLProfile;
import fr.lukyrouge.deneria.logging.CustomLogger;
import fr.lukyrouge.deneria.core.MatchManager;
import fr.lukyrouge.deneria.listener.CustomCommandListener;
import fr.lukyrouge.deneria.listener.JoinGuildListener;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

import static fr.lukyrouge.deneria.Constants.DISCORD_TOKEN;
import static fr.lukyrouge.deneria.Constants.OWNER_ID;

public class D3n3r1a {
    private static CustomLogger log = new CustomLogger(LoggerFactory.getLogger(D3n3r1a.class));
    public static LinkManager linkManager = new LinkManager();
    private static CommandClientBuilder client;
    public static EventWaiter waiter;
    public static MatchManager matchManager;
    public static VoiceChannelManager voiceChannelManager;
    public static JDA jda;

    public static void main(String[] args) throws Exception {
        setup();
    }

    private static void setup() throws Exception {
//        log.info("Starting...");
        setupClient();
        setupManagers();
        setupCommands();

        setupLeagueOfLegends();

        jda = new JDABuilder(AccountType.BOT).setToken(DISCORD_TOKEN).setStatus(OnlineStatus.DO_NOT_DISTURB).setGame(Game.playing("Loading... uwu")).addEventListener(waiter).addEventListener(client.build()).addEventListener(matchManager).buildAsync();
        jda.addEventListener(new JoinGuildListener());
        jda.addEventListener(new LinkManager());
//        jda.addEventListener(new CommandListener());
        log.info("Started and ready. Currently in: " + jda.awaitReady().getGuilds().size() + " guilds.");
    }

    private static void setupLeagueOfLegends() throws Exception {
        LeagueOfLegends.initVersion();
        LeagueOfLegends.initChamp();
        LeagueOfLegends.initSummoners();
    }

    private static void setupClient() {
        client = new CommandClientBuilder();
        client.useDefaultGame();
        client.setPrefix("d!");
        client.setEmojis("✅", "⚠", "❌");
        client.setOwnerId(OWNER_ID);
        client.setAlternativePrefix("deneria");
        client.setListener(new CustomCommandListener());

    }

    private static void setupCommands() {
        waiter = new EventWaiter();
        CommandRegister cmdreg = new CommandRegister(client);
        cmdreg.registerCommands();
    }

    private static void setupManagers() {
        matchManager = new MatchManager();
        voiceChannelManager = new VoiceChannelManager();
        Thread vcm = new Thread(voiceChannelManager);
        vcm.start();
    }
}
